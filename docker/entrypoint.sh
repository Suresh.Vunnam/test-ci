#!/bin/bash
function=$1
case $function in
  create_change_set)
     aws cloudformation create-change-set \
    --stack-name $STACK_NAME \
    --template-url ${TEMPLATE_URL} \
    --change-set-name $CHANGE_SET_NAME \
    --change-set-type UPDATE \
    --parameters $PARAMETERS_STRING \
    --capabilities CAPABILITY_NAMED_IAM --region ${REGION}
  ;;
  execute_change_set)
    STATUS=(`aws cloudformation describe-change-set --change-set-name $CHANGE_SET_NAME \
       --stack-name $STACK_NAME --region ${REGION} --query Status --output text`)
    if [ "$STATUS" != "FAILED" ]; then
      aws cloudformation execute-change-set --stack-name $STACK_NAME --change-set-name $CHANGE_SET_NAME --region ${REGION}
    fi
  ;;
  deploy)
    aws cloudformation deploy \
    --stack-name $STACK_NAME \
    --template-url ${TEMPLATE_URL}  \
    --no-fail-on-empty-changeset \
    --parameter-overrides \
      $PARAMETER_OVERRIDES_STRING
    --capabilities CAPABILITY_NAMED_IAM 
    /cfn-wait.sh $STACK_NAME ${REGION}
  ;;
  shutdown)
    aws cloudformation delete-stack --stack-name $STACK_NAME --region ${REGION}
    /cfn-wait.sh $STACK_NAME ${REGION}
  ;;
  *)
    echo "you need to set a valid phase"
esac   
